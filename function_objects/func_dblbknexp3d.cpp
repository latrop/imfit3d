/* FILE: func_brokenexp3d.cpp ------------------------------------------ */
/* 
 *
 *   Experimental function object class for a 3D exponential disk (luminosity
 * density = radial broken exponential with scale lengths h1 and h2, break
 * radius r_b, and vertical vertical sech^(2/n) profile with scale height h_z, 
 * seen at position angle PA and inclination inc.
 *   
 *   BASIC IDEA:
 *      Setup() is called as the first part of invoking the function;
 *      it pre-computes various things that don't depend on x and y.
 *      GetValue() then completes the calculation, using the actual value
 *      of x and y, and returns the result.
 *      So for an image, we expect the user to call Setup() once at
 *      the start, then loop through the pixels of the image, calling
 *      GetValue() to compute the function results for each pixel coordinate
 *      (x,y).
 *
 *   NOTE: Currently, we assume input PA is in *degrees* [and then we
 * convert it to radians] relative to +x axis.
 *
 *   MODIFICATION HISTORY:
 *     [v0.2]: 10 Aug 2013: Modified to use sech^(2/n) instead of simple exponential
 * as vertical profile.
 *     [v0.1]: 25 Oct 2012: Created (as modification of func_expdisk3d.cpp).
 */

// Copyright 2012--2016 by Peter Erwin.
// 
// This file is part of Imfit.
// 
// Imfit is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your
// option) any later version.
// 
// Imfit is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
// 
// You should have received a copy of the GNU General Public License along
// with Imfit.  If not, see <http://www.gnu.org/licenses/>.



/* ------------------------ Include Files (Header Files )--------------- */
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <string>
#include <gsl/gsl_errno.h>

#include "func_dblbknexp3d.h"
#include "integrator.h"


using namespace std;

/* ---------------- Definitions ---------------------------------------- */
const int   N_PARAMS = 10;
const char  PARAM_LABELS[][20] = {"PA", "inc", "J_0", "h1", "h2", "h3", "r_break1", "r_break2", "n", "z_0"};
const char  FUNCTION_NAME[] = "DblBknExp3D function";
const double  DEG2RAD = 0.017453292519943295;
const int  SUBSAMPLE_R = 10;

const double  COSH_LIMIT = 100.0;

const double  INTEGRATION_MULTIPLIER = 20;

const char DblBknExp3D::className[] = "DblBknExp3D";


/* ---------------- Local Functions ------------------------------------ */

double LuminosityDensityBED3( double s, void *params );




/* ---------------- CONSTRUCTOR ---------------------------------------- */

DblBknExp3D::DblBknExp3D( )
{
  string  paramName;
  
  nParams = N_PARAMS;
  functionName = FUNCTION_NAME;
  shortFunctionName = className;

  // Set up the vector of parameter labels
  for (int i = 0; i < nParams; i++) {
    paramName = PARAM_LABELS[i];
    parameterLabels.push_back(paramName);
  }

  // Stuff related to GSL integration  
  gsl_set_error_handler_off();
  F.function = LuminosityDensityBED3;
  
  doSubsampling = false;
}


/* ---------------- PUBLIC METHOD: Setup ------------------------------- */

void DblBknExp3D::Setup( double params[], int offsetIndex, double xc, double yc )
{
  x0 = xc;
  y0 = yc;
  PA = params[0 + offsetIndex];
  inclination = params[1 + offsetIndex];
  J_0 = params[2 + offsetIndex ];
  h1 = params[3 + offsetIndex ];
  h2 = params[4 + offsetIndex ];
  h3 = params[5 + offsetIndex ];
  r_b1 = params[6 + offsetIndex ];
  r_b2 = params[7 + offsetIndex ];
  n = params[8 + offsetIndex ];
  z_0 = params[9 + offsetIndex ];

  // pre-compute useful things for this round of invoking the function
  // convert PA to +x-axis reference
  PA_rad = (PA + 90.0) * DEG2RAD;
  cosPA = cos(PA_rad);
  sinPA = sin(PA_rad);
  inc_rad = inclination * DEG2RAD;
  cosInc = cos(inc_rad);
  sinInc = sin(inc_rad);
  
  // broken-exponential stuff
  J_1 = J_0 * exp(r_b1/h2 - r_b1/h1);
  J_2 = J_1 * exp(r_b2/h3 - r_b2/h2);

  
  // vertical-profile stuff
  alphaVert = 2.0/n;
  scaledZ0 = alphaVert*z_0;
  two_to_alpha = pow(2.0, alphaVert);
}


/* ---------------- PUBLIC METHOD: GetValue ---------------------------- */

double DblBknExp3D::GetValue( double x, double y )
{
  double  x_diff = x - x0;
  double  y_diff = y - y0;
  double  xp, yp, x_d0, y_d0, z_d0, totalIntensity;
  double  integLimit;
  double  xyParameters[17];
  
  // Calculate x,y in component (projected sky) reference frame
  xp = x_diff*cosPA + y_diff*sinPA;
  yp = -x_diff*sinPA + y_diff*cosPA;

  // Calculate (x,y,z)_start in component's native xyz reference frame, corresponding to
  // intersection of line-of-sight ray with projected sky frame
  x_d0 = xp;
  y_d0 = yp * cosInc;
  z_d0 = yp * sinInc;

  // Set up parameter vector for the integration (everything that stays unchanged
  // for this particular xp,yp location)
  xyParameters[0] = x_d0;
  xyParameters[1] = y_d0;
  xyParameters[2] = z_d0;
  xyParameters[3] = cosInc;
  xyParameters[4] = sinInc;
  xyParameters[5] = J_0;
  xyParameters[6] = J_1;
  xyParameters[7] = J_2;
  xyParameters[8] = h1;
  xyParameters[9] = h2;
  xyParameters[10] = h3;
  xyParameters[11] = r_b1;
  xyParameters[12] = r_b2;
  xyParameters[13] = z_0;
  xyParameters[14] = scaledZ0;
  xyParameters[15] = two_to_alpha;
  xyParameters[16] = alphaVert;
  F.params = xyParameters;

  // integrate out to +/- integLimit, which is larger of (multiple of break radius)
  // and (multiple of h2)
  // (NOTE: it seems like it would be faster to precalculate integLimit in the
  // Setup() call above; for some reason doing it that way makes the whole thing
  // take ~ 4 times longer!)
  integLimit = fmax(INTEGRATION_MULTIPLIER * r_b2, INTEGRATION_MULTIPLIER * h2);
  totalIntensity = Integrate(F, -integLimit, integLimit);

  return totalIntensity;
}







/* ----------------------------- OTHER FUNCTIONS -------------------------------- */


/* Compute luminosity density for a location (x_d,y_d,z_d) which is at line-of-sight 
 * distance s from start point (x_d0, y_d0, z_d0), where midplane of component (e.g.,
 * disk of galaxy) is oriented at angle (90 - inclination) to the line of sight vector. 
 */ 
double LuminosityDensityBED3( double s, void *params )
{
  double  y_d, z_d, z, R, J_rad, lumDensity;
  double  verticalScaling, sech;
  double  *paramsVect = (double *)params;
  double x_d0 = paramsVect[0];
  double y_d0 = paramsVect[1];
  double z_d0 = paramsVect[2];
  double cosInc = paramsVect[3];
  double sinInc = paramsVect[4];
  double J_0 = paramsVect[5];
  double J_1 = paramsVect[6];
  double J_2 = paramsVect[7];
  double h1 = paramsVect[8];
  double h2 = paramsVect[9];
  double h3 = paramsVect[10];
  double r_b1 = paramsVect[11];
  double r_b2 = paramsVect[12];
  double z_0 = paramsVect[13];
  double scaledZ0 = paramsVect[14];
  double two_to_alpha = paramsVect[15];
  double alphaVert = paramsVect[16];
  
  // Given s and the pre-defined parameters, determine our 3D location (x_d,y_d,z_d)
  // [by construction, x_d = x_d0]
  y_d = y_d0 + s*sinInc;
  z_d = z_d0 - s*cosInc;
  
  // Convert 3D Cartesian coordinate to R,z coordinate
  R = sqrt(x_d0*x_d0 + y_d*y_d);
  z = fabs(z_d);

  // Calculate radial component
  // check for possible overflow in exponentiation if r >> r_b, and re-route around it:
  if (R <= r_b1)
    J_rad = J_0 * exp(-R/h1);
  else if ((R > r_b1) && (R < r_b2))
        J_rad = J_1 * exp(-R/h2);
  else 
    J_rad = J_2 * exp(-R/h3);

  // if combination of n*z/z_0 is large enough, switch to simple exponential
  if ((z/scaledZ0) > COSH_LIMIT)
    verticalScaling = two_to_alpha * exp(-z/z_0);
  else {
    sech = 1.0 / cosh(z/scaledZ0);
    verticalScaling = pow(sech, alphaVert);
  }

  lumDensity = J_rad * verticalScaling;
  return lumDensity;
}



/* END OF FILE: func_brokenexp3d.cpp ----------------------------------- */
