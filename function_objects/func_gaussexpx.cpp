/* FILE: func_GaussExpX.cpp ----------------------------------------------- */
/* VERSION 0.3
 *
 *   Function object class for a Sersic function, with constant
 * ellipticity and position angle (pure elliptical, not generalized).
 *   
 *   BASIC IDEA:
 *      Setup() is called as the first part of invoking the function;
 *      it pre-computes various things that don't depend on x and y.
 *      GetValue() then completes the calculation, using the actual value
 *      of x and y, and returns the result.
 *      So for an image, we expect the user to call Setup() once at
 *      the start, then loop through the pixels of the image, calling
 *      GetValue() to compute the function results for each pixel coordinate
 *      (x,y).
 *
 *   NOTE: Currently, we assume input PA is in *degrees* [and then we
 * convert it to radians] relative to +x axis.
 *
 *   MODIFICATION HISTORY:
 *     [v0.4]  20--26 Mar 2010: Preliminary support for pixel subsampling.
 *     [v0.3]: 21 Jan 2010: Modified to treat x0,y0 as separate inputs.
 *     [v0.2]: 28 Nov 2009: Updated to new FunctionObject interface.
 *     [v0.1]: 19 Nov 2009: Created (as modification of func_exp.cpp.
       [v_gaussexpx]: AS scrutinese func_sersic.cpp   
 */

// Copyright 2010, 2011, 2012, 2013 by Peter Erwin.
// 
// This file is part of Imfit.
// 
// Imfit is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your
// option) any later version.
// 
// Imfit is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
// 
// You should have received a copy of the GNU General Public License along
// with Imfit.  If not, see <http://www.gnu.org/licenses/>.



/* ------------------------ Include Files (Header Files )--------------- */
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <string>
#include <algorithm>
#include <iostream>
#include <gsl/gsl_sf.h>
#include "func_gaussexpx.h"

using namespace std;


/* ---------------- Definitions ---------------------------------------- */
const int  N_PARAMS = 10;
const char  PARAM_LABELS[][20] = {"PA", "ell", "I0", "h_inner", "n_along", "h_outer", "r_break", "n_across", "r_e_across", "asymm"};
const char  FUNCTION_NAME[] = "GaussExpX function";
//const char SHORT_FUNCTION_NAME[] = "GaussxBrokenExp";
const double  DEG2RAD = 0.017453292519943295;
const int  SUBSAMPLE_R = 10;

const char GaussExpX::className[] = "GaussExpX";

const double  A0_M03 = 0.01945;
const double  A1_M03 = -0.8902;
const double  A2_M03 = 10.95;
const double  A3_M03 = -19.67;
const double  A4_M03 = 13.43;


/* ---------------- CONSTRUCTOR ---------------------------------------- */

GaussExpX::GaussExpX( )
{
  string  paramName;
  
  nParams = N_PARAMS;
  functionName = FUNCTION_NAME;
  shortFunctionName = className;
  // Set up the vector of parameter labels
  for (int i = 0; i < nParams; i++) {
    paramName = PARAM_LABELS[i];
    parameterLabels.push_back(paramName);
  }
  
  doSubsampling = true;
}


/* ---------------- PUBLIC METHOD: Setup ------------------------------- */

void GaussExpX::Setup( double params[], int offsetIndex, double xc, double yc )
{
  x0 = xc;
  y0 = yc;
  PA =  params[0 + offsetIndex];
  ell =  params[1 + offsetIndex];
  I0 =  params[2 + offsetIndex ];
  h_inner = params[3 + offsetIndex ];
  n_along = params[4 + offsetIndex];
  h_outer = params[5 + offsetIndex ];
  r_break = params[6 + offsetIndex ];
  n_across =   params[7 + offsetIndex ];
  r_e_across = params[8 + offsetIndex ];
  asymm = params[9 + offsetIndex ];
  // pre-compute useful things for this round of invoking the function
  q = 1.0 - ell;
  //I02 = I0 * exp(-r_break/h_inner + r_break/h_outer);  // Outer disk central surface brightness
  
  r_e_across_Squared =  r_e_across*r_e_across;
  if (r_e_across  > 0.36){
    bn2 = 2*r_e_across - 0.333333333333333 + 0.009876543209876543/r_e_across
      + 0.0018028610621203215/r_e_across_Squared + 0.00011409410586365319/(r_e_across_Squared*r_e_across)
         - 7.1510122958919723e-05/(r_e_across_Squared*r_e_across_Squared);
  }
   else {
    bn2 = A0_M03 + A1_M03*r_e_across + A2_M03*r_e_across_Squared + A3_M03*r_e_across_Squared*
                                     r_e_across+ A4_M03*r_e_across_Squared*r_e_across_Squared;
  }
  invn2 = 1.0 / r_e_across;
  

  n1Squared = n_along * n_along;
  if (n_along> 0.36){
    bn1 = 2*n_along - 0.333333333333333 + 0.009876543209876543/n_along
         + 0.0018028610621203215/n1Squared + 0.00011409410586365319/(n1Squared*n_along)
         - 7.1510122958919723e-05/(n1Squared*n1Squared);
  }
   else {
    bn1 = A0_M03 + A1_M03*n_along + A2_M03*n1Squared + A3_M03*n1Squared*n_along + A4_M03*n1Squared*n1Squared;
  }
  invn1 = 1.0 / n_along;
  I02 = I0 * exp( -bn1 * (pow((r_break/h_inner), invn1) - 1.0) + r_break/h_outer);
  k = ell;
  k1 = 1.0/k;
  k2 = k*k;
  if (n_across < 1) {
    n_across2 = n_across*n_across;
  } else {
    n_across2 = pow(2.0-n_across, 2);
  }
  
  
  e = sqrt(1.0 - n_across2);
  t_cross = atan(k);
  t_cross_orth = t_cross;
  k_orth = -k;
  k_orth2 = k2;
  sqrt2 = sqrt(2.0);

  PA_rad = (PA - 90.0) * DEG2RAD;
  cosPA = cos(PA_rad);
  sinPA = sin(PA_rad);
  

  r_e_across2 = r_e_across*r_e_across;
  r_e_across2_2 = 2.0*r_e_across2;
  h_outer2 = h_outer*h_outer;
  sqrtpi = sqrt(acos(-1.0));
  h_outer05 = h_outer/2.0;
  h_outer05sqrtpi = h_outer05/sqrtpi;

  
  k_n_across = -tan(n_across * DEG2RAD);
  
  
}


/* ---------------- PRIVATE METHOD: CalculateIntensity ----------------- */
// This function calculates the intensity for a Sersic function at radius r,
// with the various parameters and derived values (n, b_n, r_e, etc.)
// pre-calculated by Setup().

/* ---------------- PUBLIC METHOD: GetValue ---------------------------- */
// This function calculates and returns the intensity value for a pixel with
// coordinates (x,y), including pixel subsampling if necessary (and if subsampling
// is turned on). The CalculateIntensity() function is called for the actual
// intensity calculation.

double GaussExpX::GetValue( double x, double y )
{


  // 0.0 aux things
  double  x_diff = x - x0;
  double  y_diff = y - y0;
  

  


  
  double k2 = k*k;
  double k_orth  = -k;
  double k_orth2 = k2;
  double  xp, yp_scaled, r, totalIntensity;
  double lobeDist_R, lobeDist_z, xOnLobe, yOnLobe, centerDist, factorAlongLobe, factorAcrossLobe, c;// k;
  double lobeDist_elliptical;
  int  nSubsamples;


  // 1.0 system rotation
  double x_rotated = x_diff *  cosPA + y_diff * sinPA;
  double y_rotated = -sinPA*x_diff + cosPA * y_diff;
  x_diff = x_rotated;
  y_diff = y_rotated;
  
  x_diff = fabs(x_diff);
  y_diff = fabs(y_diff);

  double y_diff2 = y_diff*y_diff;
  double x_diff2 = x_diff*x_diff;
  

	


  double arg_exp, arg_erfc, arg_erfc2, arg_erfc3, arg_erfc5;
  double t0;
  

  // 1.1 trat accurately near-zero difference
  if (fabs(x_diff) > 2e-17) {
    t0 = atan(fabs(y_diff/x_diff));
  } else {
     t0 = acos(0);
  }
  
  


  lobeDist_R = 0;
  lobeDist_z = 0;
  lobeDist_elliptical = 0;
  

  // 2.0 major axis of ellipses
  double a;
  if (n_across < 1) {
    a=sqrt(x_diff2 + y_diff2/n_across2);
  } else {
    a=sqrt(x_diff2 + y_diff2*n_across2);   
  }


  
  // 3.0 calc distances

  lobeDist_R = x_diff - pow(y_diff/k, 1.0/r_break);
  lobeDist_z = y_diff - k*pow(fabs(x_diff), r_break);


  // 4.0 quadrantss
  double x_cross, k_signed, LobeDist;
  if (y_diff > 0) {
    k_signed = k;
  } else {
    k_signed = -k;
  }

  // 5.0 point of intersection of the ellipse and the ray of X-shape
  x_cross = fabs(x_diff) + y_diff * k_signed * r_break;
  x_cross /= k_signed*k_signed*r_break*r_break + 1;
  LobeDist = sqrt(pow(fabs(x_diff)-x_cross,2) + pow(y_diff - k_signed*pow(x_cross, r_break), 2) );
 


  // 6  .0 decay factor above the rays
  
  if (lobeDist_R >= 0) { 
    factorAcrossLobe = 1;
  } else {   
    factorAcrossLobe = exp(-pow(fabs(lobeDist_R)/asymm,1.0/r_e_across)); 
  }


  // 7.0 actual calculation of the intensity  
  factorAcrossLobe +=  h_outer*exp(-LobeDist*LobeDist/(r_e_across*r_e_across));
  centerDist = a;
  
  factorAlongLobe = I0 * exp( -bn1 * (pow((centerDist/h_inner), invn1) - 1.0));


  totalIntensity = factorAlongLobe * factorAcrossLobe;


  // 8.0 debugging string
  if (totalIntensity != totalIntensity ) std::cerr << arg_exp << " " << arg_erfc << " " << factorAcrossLobe << 
   " " <<  factorAlongLobe << " " << centerDist <<" " << bn1 <<" " << h_inner <<" " << invn1 << " "<< centerDist/h_inner  
    << " " << pow((centerDist/h_inner), invn1) << " " << -bn1 * (pow((centerDist/h_inner), invn1) - 1.0) <<" " << "\n";

  return totalIntensity;
}

/* END OF FILE: func_sersic.cpp ---------------------------------------- */
