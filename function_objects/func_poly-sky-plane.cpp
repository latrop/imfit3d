/* FILE: func_poly-sky-plane.cpp ---------------------------------------------- */
/*
 *   This is a derived class which provides for a sky background modeled as an
 * inclined ("polynomial") plane.
 *
 *   Inspired by the InclinedFlatSky function of Dan Prole (djampro).
 *
 */

// Copyright 2020 by Peter Erwin.
//
// This file is part of Imfit.
//
// Imfit is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// Imfit is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License along
// with Imfit.  If not, see <http://www.gnu.org/licenses/>.


/* ------------------------ Include Files (Header Files )--------------- */
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <string>

#include "func_poly-sky-plane.h"

using namespace std;


/* ---------------- Definitions ---------------------------------------- */
const int  N_PARAMS = 10;
const char  PARAM_LABELS[][20] = {"I_0", "m_x", "m_y", "m_x2", "m_y2", "m_xy", "m_x3", "m_y3", "m_x2y", "m_xy2"};
const char  FUNCTION_NAME[] = "Poly sky-plane background function";

const char PolySkyPlane::className[] = "PolySkyPlane";


/* ---------------- CONSTRUCTOR ---------------------------------------- */

PolySkyPlane::PolySkyPlane( )
{
  string  paramName;
  nParams = N_PARAMS;

  functionName = FUNCTION_NAME;
  shortFunctionName = className;

  // Set up the vector of parameter labels
  for (int i = 0; i < nParams; i++) {
    paramName = PARAM_LABELS[i];
    parameterLabels.push_back(paramName);
  }

  doSubsampling = true;
}


/* ---------------- PUBLIC METHOD: Setup ------------------------------- */

void PolySkyPlane::Setup( double params[], int offsetIndex, double xc, double yc )
{
  x0 = xc;
  y0 = yc;
  I_0 = params[0 + offsetIndex ];
  m_x = params[1 + offsetIndex ];   // slope in x-direction
  m_y = params[2 + offsetIndex ];   // slope in y-direction
  m_x2 = params[3 + offsetIndex ];
  m_y2 = params[4 + offsetIndex ];
  m_xy = params[5 + offsetIndex ];
  m_x3 = params[6 + offsetIndex ];
  m_y3 = params[7 + offsetIndex ];
  m_x2y = params[8 + offsetIndex ];
  m_xy2 = params[9 + offsetIndex ];
}


/* ---------------- PUBLIC METHOD: GetValue ---------------------------- */

double PolySkyPlane::GetValue( double x, double y )
{
  double  x_diff = (x - x0) / x0;
  double  y_diff = (y - y0) / y0;
  double i_tot = I_0;
  i_tot = m_x*x_diff + m_y*y_diff + m_x2 * pow(x_diff, 2) + m_y2 * pow(y_diff, 2);
  i_tot = i_tot + m_xy * x_diff * y_diff + m_x3*pow(x_diff, 3) + m_y3 * pow(y_diff, 3);
  i_tot = i_tot + m_x2y * pow(x_diff, 2) * y_diff + m_xy2 * x_diff * pow(y_diff, 2);
  return i_tot;
}


/* ---------------- PUBLIC METHOD: IsBackground ------------------------ */

// bool PolySkyPlane::IsBackground( )
// {
//   return true;
// }



/* END OF FILE: func_poly-sky-plane.cpp ------------------------------ */
