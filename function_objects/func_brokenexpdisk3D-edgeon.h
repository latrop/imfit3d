/* Class interface definition for func_brokenexpdisk3D-edgeon.cpp
 *
 *
 * PARAMETERS:
 * x0 = xc;   -- center of component (pixels, x)
 * y0 = yc;   -- center of component (pixels, y)
 * PA = params[0 + offsetIndex];     -- PA of component line of nodes, rel. to image +x axis
 * inclination = params[1 + offsetIndex];  -- inclination to line of sight (i=0 for face-on)
 * J_0 = params[2 + offsetIndex ];   -- central luminosity density (ADU)
 * h1 = params[3 + offsetIndex ];    -- inner exp. scale length (pixels)
 * h2 = params[4 + offsetIndex ];    -- outer exp. scale length (pixels)
 * r_b = params[5 + offsetIndex ];   -- break radius (pixels)
 * n = params[7 + offsetIndex ];     -- exponent used in sech vertical function
 * z_0 = params[8 + offsetIndex ];   -- vertical scale height
 *
 */


// CLASS BknEdgeOn:

#include <string>
#include "function_object.h"

using namespace std;

class BknEdgeOn : public FunctionObject {
    // the following static constant will be defined/initialized in the .cpp file
    static const char className[];

public:
    // Constructor
    BknEdgeOn();

    // redefined method/member function:
    void Setup(double params[], int offsetIndex, double xc, double yc);

    double GetValue(double x, double y);
    // No destructor for now

    // class method for returning official short name of class
    static void GetClassShortName(string &classname) { classname = className; };


private:
    double x0, y0, PA, J_0, h1, h2, r_b, n, z_0;   // parameters
    double PA_rad, cosPA, sinPA;   // other useful quantities
    double alphaVert, scaledZ0, two_to_alpha;
    double J_0_2, J_0_2K;
};

