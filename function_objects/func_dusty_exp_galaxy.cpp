/* FILE: func_expdisk3d.cpp -------------------------------------------- */
/* 
 *   Experimental function object class for a 3D exponential disk (luminosity
 * density = radial exponential with scale length h and vertical sech^(2/n) profile
 * with scale heigh h_z), seen at position angle PA and inclination inc.
 *   
 *   BASIC IDEA:
 *      Setup() is called as the first part of invoking the function;
 *      it pre-computes various things that don't depend on x and y.
 *      GetValue() then completes the calculation, using the actual value
 *      of x and y, and returns the result.
 *      So for an image, we expect the user to call Setup() once at
 *      the start, then loop through the pixels of the image, calling
 *      GetValue() to compute the function results for each pixel coordinate
 *      (x,y).
 *
 *   NOTE: Currently, we assume input PA is in *degrees* [and then we
 * convert it to radians] relative to +x axis.
 *
 *   MODIFICATION HISTORY:
 *     [v0.2]{ 24 Aug: Correct implementation of line-of-sight integration (using
 *   sky plane passing through component center as s=0 start point for integration).
 *     [v0.1]: 18--19 Aug 2012: Created (as modification of func_exp.cpp.
 */

// Copyright 2012--2016 by Peter Erwin.
// 
// This file is part of Imfit.
// 
// Imfit is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your
// option) any later version.
// 
// Imfit is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
// 
// You should have received a copy of the GNU General Public License along
// with Imfit.  If not, see <http://www.gnu.org/licenses/>.




// Working Python code:

// def ExpFunc( s, x_d0, y_d0, inc_rad, p ):
// 
// 	I0 = p[0]
// 	h = p[1]
// 	h_z = p[2]
// 	
// 	x_d = x_d0
// 	y_d = y_d0 + s*math.sin(inc_rad)
// 	z_d = -s*math.cos(inc_rad)
// 	
// 	R = math.sqrt(x_d**2 + y_d**2)
// 	z = abs(z_d)
// 	
// 	return I0 * math.exp(-R/h)*math.exp(-z/h_z)
// 
// 
// 
// def DoIntegration( x_d0, y_d0, inc_rad, someFunc, params ):
// 	
// 	I,err = integrate.quad(someFunc, -INT_LIMIT, INT_LIMIT, args=(x_d0,y_d0,inc_rad,params))



/* ------------------------ Include Files (Header Files )--------------- */
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <string>
//#include <gsl/gsl_errno.h>
#include <gsl/gsl_sf_gamma.h>

#include "func_dusty_exp_galaxy.h"
#include "integrator.h"
#include "helper_funcs.h"


using namespace std;

/* ---------------- Definitions ---------------------------------------- */
const int   N_PARAMS = 14;
const char  PARAM_LABELS[][20] = {"PA", "ell", "n_bulge", "I_e", "r_e", "inc", "J_0", "h", "n_disk", "z_0", "J_0_dust", "h_dust", "n_dust", "z_0_dust"};
const char  FUNCTION_NAME[] = "DustyExp3DGalaxy function";
const double  DEG2RAD = 0.017453292519943295;
const int  SUBSAMPLE_R = 10;

const double  COSH_LIMIT = 100.0;

const double  INTEGRATION_MULTIPLIER = 20;

// const double weights[] = {0.17132449, 0.36076157, 0.46791393, 0.46791393, 0.36076157, 0.17132449};
// const double roots[] = {-0.93246951, -0.66120939, -0.23861919, 0.23861919, 0.66120939, 0.93246951};
const double weights[] = {0.23692689, 0.47862867, 0.56888889, 0.47862867, 0.23692689};
const double roots[] = {-0.90617985, -0.53846931,  0.        ,  0.53846931,  0.90617985};
const int n_gauss = 4;

const double sersic_coeffs[11][11] =
  {
    {5.017e-3, 1.573e-3, -7.175e-2, 1.256e-1, 4.047e-1, -1.191, 2.455e-1, 8.650e-1, 1.178, -2.771, 1.209},
    {-4.507e-3, -8.623e-3, 5.430e-2, 2.298e-1, -9.349e-1, -3.113e-1, 5.052, -7.980, 5.065, -1.162, 0.0},
    {-4.251e-2, 6.921e-2, 2.706e-1, -1.134, 1.318, 7.078e-1, -3.093, 2.664, -7.604e-1, 0.0, 0.0},
    {1.373e-2, -2.444e-2, -8.324e-2, 2.795e-1, -1.923e-1, -2.219e-1, 3.670e-1, -1.381e-1, 0.0, 0.0, 0.0},
    {1.428e-3, -7.563e-3, 2.897e-4, 5.385e-2, -1.248e-1, 1.200e-1, -4.310e-2, 0.0, 0.0, 0.0, 0.0},
    {-1.388e-3, 3.261e-3, 1.615e-3, -1.112e-2, 1.272e-2, -5.059e-3, 0.0, 0.0, 0.0, 0.0, 0.0},
    {-9.613e-5, 5.901e-4, -4.636e-4, -4.867e-4, 4.043e-4, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
    {9.505e-5, -1.801e-4, 1.074e-5, 5.734e-5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
    {4.306e-6, -2.496e-5, 2.464e-5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
    {-2.924e-6, 4.229e-6, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
    {-2.117e-8, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
  };

// const double weights[] = {0.01761401, 0.04060143, 0.06267205, 0.08327674, 0.10193012,
// 			  0.11819453, 0.13168864, 0.14209611, 0.14917299, 0.15275339,
// 			  0.15275339, 0.14917299, 0.14209611, 0.13168864, 0.11819453,
// 			  0.10193012, 0.08327674, 0.06267205, 0.04060143, 0.01761401};
// const double roots[] = {-0.9931286 , -0.96397193, -0.91223443, -0.83911697, -0.74633191,
// 			-0.63605368, -0.510867  , -0.37370609, -0.22778585, -0.07652652,
// 			0.07652652,  0.22778585,  0.37370609,  0.510867  ,  0.63605368,
// 			0.74633191,  0.83911697,  0.91223443,  0.96397193,  0.9931286};
// const int n_gauss = 19;

const char DustyExp3DGalaxy::className[] = "DustyExp3DGalaxy";


/* ---------------- Local Functions ------------------------------------ */

double CalculateSersicDensity( double r, double *params );
double LuminosityDensityD( double s, double *params );
double LuminosityDensityB( double s, double *params );
double ComputeChunk(double a, double b, double *params, double *params_dust, double * params_bulge);


/* ---------------- CONSTRUCTOR ---------------------------------------- */

DustyExp3DGalaxy::DustyExp3DGalaxy( )
{
  string  paramName;
  
  nParams = N_PARAMS;
  functionName = FUNCTION_NAME;
  shortFunctionName = className;

  // Set up the vector of parameter labels
  for (int i = 0; i < nParams; i++) {
    paramName = PARAM_LABELS[i];
    parameterLabels.push_back(paramName);
  }

  // Stuff related to GSL integration  
  //gsl_set_error_handler_off();
  
  doSubsampling = false;
}


/* ---------------- PUBLIC METHOD: Setup ------------------------------- */

void DustyExp3DGalaxy::Setup( double params[], int offsetIndex, double xc, double yc )
{
  x0 = xc;
  y0 = yc;
  PA = params[0 + offsetIndex];
  ell = params[1 + offsetIndex];
  n_bulge = params[2 + offsetIndex];
  I_e = params[3 + offsetIndex];
  r_e = params[4 + offsetIndex];
  inclination = params[5 + offsetIndex];
  J_0 = params[6 + offsetIndex ];
  h = params[7 + offsetIndex ];
  n_disk = params[8 + offsetIndex ];
  z_0 = params[9 + offsetIndex ];
  J_0_dust = params[10 + offsetIndex ];
  h_dust = params[11 + offsetIndex ];
  n_dust = params[12 + offsetIndex ];
  z_0_dust = params[13 + offsetIndex ];

  // pre-compute useful things for this round of invoking the function
  // convert PA to +x-axis reference
  q = 1.0 - ell;
  bn = Calculate_bn(n_bulge);
  invn = 1.0 / n_bulge;
  
  PA_rad = (PA + 90.0) * DEG2RAD;
  cosPA = cos(PA_rad);
  sinPA = sin(PA_rad);
  inc_rad = inclination * DEG2RAD;
  cosInc = cos(inc_rad);
  sinInc = sin(inc_rad);
  
  alpha = 2.0/n_disk;
  alpha_dust = 2.0/n_dust;
  scaledZ0 = alpha*z_0;
  scaledZ0_dust = alpha*z_0_dust;
  two_to_alpha = pow(2.0, alpha);
  two_to_alpha_dust = pow(2.0, alpha_dust);
}


/* ---------------- PUBLIC METHOD: GetValue ---------------------------- */

double DustyExp3DGalaxy::GetValue( double x, double y )
{
  double  x_diff = x - x0;
  double  y_diff = y - y0;
  if ((x_diff > 0) && (x_diff < 1)){
    x_diff = 1.0;
    }
  if ((y_diff > 0) && (y_diff < 1)){
    y_diff = 1.0;
    }
  double  xp, yp, x_d0, y_d0, z_d0, totalIntensity=0.0;
  double  integLimit;
  double  xyParameters[11];
  double  xyParameters_dust[11];
  double  xyParameters_bulge[12];
  
  // Calculate x,y in component (projected sky) reference frame
  xp = x_diff*cosPA + y_diff*sinPA;
  yp = -x_diff*sinPA + y_diff*cosPA;

  // Calculate (x,y,z)_start in component's native xyz reference frame, corresponding to
  // intersection of line-of-sight ray with projected sky frame
  x_d0 = xp;
  y_d0 = yp * cosInc;
  z_d0 = yp * sinInc;

  // Set up parameter vector for the integration (everything that stays unchanged
  // for this particular xp,yp location)
  xyParameters[0] = x_d0;
  xyParameters[1] = y_d0;
  xyParameters[2] = z_d0;
  xyParameters[3] = cosInc;
  xyParameters[4] = sinInc;
  xyParameters[5] = J_0;
  xyParameters[6] = h;
  xyParameters[7] = z_0;
  xyParameters[8] = scaledZ0;
  xyParameters[9] = two_to_alpha;
  xyParameters[10] = alpha;

  xyParameters_dust[0] = x_d0;
  xyParameters_dust[1] = y_d0;
  xyParameters_dust[2] = z_d0;
  xyParameters_dust[3] = cosInc;
  xyParameters_dust[4] = sinInc;
  xyParameters_dust[5] = J_0_dust;
  xyParameters_dust[6] = h_dust;
  xyParameters_dust[7] = z_0_dust;
  xyParameters_dust[8] = scaledZ0_dust;
  xyParameters_dust[9] = two_to_alpha_dust;
  xyParameters_dust[10] = alpha_dust;
  
  xyParameters_bulge[0] = x_d0;
  xyParameters_bulge[1] = y_d0;
  xyParameters_bulge[2] = z_d0;
  xyParameters_bulge[3] = cosInc;
  xyParameters_bulge[4] = sinInc;
  xyParameters_bulge[5] = q;
  xyParameters_bulge[6] = n_bulge;
  xyParameters_bulge[7] = I_e;
  xyParameters_bulge[8] = r_e;
  xyParameters_bulge[9] = invn;
  xyParameters_bulge[10] = bn;
  double pn = 1.0 - 0.6097 / n_bulge + 0.05463 / (n_bulge * n_bulge);
  xyParameters_bulge[11] = pn;
  // Central surface brightness
  double I_0 = I_e * exp(bn) / (0.761 * pow(n_bulge, -0.6684) - 0.01);
  // Central luminosity density
  xyParameters_bulge[12] =  I_0 * gsl_sf_gamma(2*n_bulge) / (2*r_e * gsl_sf_gamma(n_bulge*(3-pn)));
  // Compute integration steps
  double step1 = min(r_e, h);
  double as[7];
  double bs[7];
  as[0] = 0.0;
  bs[0] = 0.5 * step1;
  as[1] = 0.5 * step1;
  bs[1] = 1.5 * step1;
  as[2] = 1.5 * step1;
  bs[2] = 4 * step1;
  double step2 = max(4 * step1, h);
  as[3] = 4 * step1;
  bs[3] = 4 * step1 + step2;
  as[4] = 4 * step1 + step2;
  bs[4] = 4 * step1 + 3*step2;
  as[5] = 4 * step1 + 3*step2;
  bs[5] = 4 * step1 + 10 * step2;
  as[6] = 4 * step1 + 10 * step2;
  bs[6] = 4 * step1 + 20 * step2;
  // First chunk
  //printf("=====\n");
  for (int step_idx=0; step_idx<=6; step_idx++){
    //printf("%1.2f %1.2f\n", as[step_idx], bs[step_idx]);
    totalIntensity += ComputeChunk(as[step_idx], bs[step_idx], xyParameters, xyParameters_dust, xyParameters_bulge);
    totalIntensity += ComputeChunk(-bs[step_idx], -as[step_idx], xyParameters, xyParameters_dust, xyParameters_bulge);
  }
  return totalIntensity;
}


double ComputeChunk(double a, double b, double *params, double * params_dust, double * params_bulge){
  double total = 0.0, bahalf=0.5*(b-a);
  double intens = 0.0, s, dust_total;
  double a_dust, b_dust, s_dust, bahalf_dust;
  double intens_dust;
  for (int i=0; i<=n_gauss; i++){
    s = bahalf*roots[i]+0.5*(b+a);
    intens = weights[i] * (LuminosityDensityD(s, params) + LuminosityDensityB(s, params_bulge));

    dust_total = 0.0;
    if (s < 0){
      a_dust = s;
      b_dust = 0;
      bahalf_dust = 0.5*(b_dust - a_dust);
      intens_dust = 0.0;
      for (int j=0; j<=n_gauss; j++){
	s_dust = bahalf_dust*roots[j] + 0.5*(b_dust+a_dust);
	intens_dust += weights[j] * LuminosityDensityD(s_dust, params_dust);
      }
      dust_total += intens_dust * bahalf_dust;
      a_dust = 0;
      b_dust = 15*params_dust[6];
      bahalf_dust = 0.5*(b_dust - a_dust);
      intens_dust = 0.0;
      for (int j=0; j<=n_gauss; j++){
	s_dust = bahalf_dust*roots[j] + 0.5*(b_dust+a_dust);
	intens_dust += weights[j] * LuminosityDensityD(s_dust, params_dust);
      }
      dust_total += intens_dust * bahalf_dust;
    }
    else{
      a_dust = s;
      b_dust = 15*params_dust[6];
      bahalf_dust = 0.5*(b_dust - a_dust);
      intens_dust = 0.0;
      for (int j=0; j<=n_gauss; j++){
	s_dust = bahalf_dust*roots[j] + 0.5*(b_dust+a_dust);
	intens_dust += weights[j] * LuminosityDensityD(s_dust, params_dust);
      }
      dust_total += intens_dust * bahalf_dust;
    }
    total += intens * exp(-dust_total);
  }
  return total * bahalf;
}

/* ----------------------------- OTHER FUNCTIONS -------------------------------- */


/* Compute luminosity density for a location (x_d,y_d,z_d) which is at line-of-sight 
 * distance s from start point (x_d0, y_d0, z_d0), where midplane of component (e.g.,
 * disk of galaxy) is oriented at angle (90 - inclination) to the line of sight vector. 
 */ 
double LuminosityDensityD( double s, double *params )
{
  double y_d, z_d, z, R, lumDensity;
  double verticalScaling, sech;
  double *paramsVect = (double *)params;
  double x_d0 = paramsVect[0];
  double y_d0 = paramsVect[1];
  double z_d0 = paramsVect[2];
  double cosInc = paramsVect[3];
  double sinInc = paramsVect[4];
  double J_0 = paramsVect[5];
  double h = paramsVect[6];
  double z_0 = paramsVect[7];
  double scaledZ0 = paramsVect[8];
  double two_to_alpha = paramsVect[9];
  double alpha = paramsVect[10];
  
  // Given s and the pre-defined parameters, determine our 3D location (x_d,y_d,z_d)
  // [by construction, x_d = x_d0]
  y_d = y_d0 + s*sinInc;
  z_d = z_d0 - s*cosInc;
  
  // Convert 3D Cartesian coordinate to R,z coordinate
  R = sqrt(x_d0*x_d0 + y_d*y_d);
  z = fabs(z_d);
  //printf("%1.2f  %1.2f\n", s, z);
  // if combination of n*z/z_0 is large enough, switch to simple exponential
  if ((z/scaledZ0) > COSH_LIMIT)
    verticalScaling = two_to_alpha * exp(-z/z_0);
  else {
    sech = 1.0 / cosh(z/scaledZ0);
    verticalScaling = pow(sech, alpha);
  }

  lumDensity = J_0 * exp(-R/h) * verticalScaling;
  return lumDensity;
}


double LuminosityDensityB(double s, double *params)
{
  double y_d, z_d, z, R, lumDensity;
  double verticalScaling, sech;
  double *paramsVect = (double *)params;
  double x_d0 = paramsVect[0];
  double y_d0 = paramsVect[1];
  double z_d0 = paramsVect[2];
  double cosInc = paramsVect[3];
  double sinInc = paramsVect[4];
  double q = paramsVect[5];
  // Given s and the pre-defined parameters, determine our 3D location (x_d,y_d,z_d)
  // [by construction, x_d = x_d0]
  y_d = y_d0 + s*sinInc;
  z_d = z_d0 - s*cosInc;
  
  // Convert 3D Cartesian coordinate to R,z coordinate
  R = sqrt(x_d0*x_d0 + y_d*y_d + z_d * z_d / q / q);
  lumDensity = CalculateSersicDensity(R, params);
  return lumDensity;
}


double CalculateSersicDensity( double r, double * params)
{
  double *paramsVect = (double *)params;
  double n_bulge = paramsVect[6];
  double I_e = paramsVect[7];
  double r_e = paramsVect[8];
  double invn = paramsVect[9];
  double bn = paramsVect[10];
  double pn = paramsVect[11];
  double rre = r/r_e;
  double rho0=paramsVect[12], rho_lgm;
  double factor;
  rho_lgm = rho0 * pow(rre, -pn) * exp(-bn * pow(rre, invn));
  factor = 0.0;
  int i, j;
  for (i=0; i<=10; i++){
    for (j=0; j<=10-i; j++){
      factor += sersic_coeffs[i][j] * pow(log10(rre), i) * pow(log10(n_bulge), j);
    }
  }
  return rho_lgm * pow(10, factor);
}





