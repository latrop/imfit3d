/* FILE: func_double-brokenexpdisk3D-edgeon.cpp ------------------------------------------ */




/* ------------------------ Include Files (Header Files )--------------- */
#include <cmath>
#include <string>
#include "func_double-brokenexpdisk3D-edgeon.h"

using namespace std;

/* ---------------- Definitions ---------------------------------------- */
const int N_PARAMS = 9;
const char PARAM_LABELS[][20] = {"PA", "J_0", "h1", "h2", "h3", "r_break_1", "r_break_2", "n", "z_0"};
const char FUNCTION_NAME[] = "DblBknEdgeOn function";
const double DEG2RAD = 0.017453292519943295;

const double weights[] = {0.00202681, 0.00471273, 0.00738993, 0.01004756, 0.01267817,
                          0.01527462, 0.0178299 , 0.02033712, 0.02278952, 0.02518048,
                          0.02750356, 0.02975249, 0.03192122, 0.03400389, 0.0359949 ,
                          0.03788887, 0.0396807 , 0.04136555, 0.04293889, 0.04439648,
                          0.04573438, 0.04694899, 0.04803703, 0.04899558, 0.04982204,
                          0.05051418, 0.05107016, 0.05148845, 0.05176794, 0.05190788,
                          0.05190788, 0.05176794, 0.05148845, 0.05107016, 0.05051418,
                          0.04982204, 0.04899558, 0.04803703, 0.04694899, 0.04573438,
                          0.04439648, 0.04293889, 0.04136555, 0.0396807 , 0.03788887,
                          0.0359949 , 0.03400389, 0.03192122, 0.02975249, 0.02750356,
                          0.02518048, 0.02278952, 0.02033712, 0.0178299 , 0.01527462,
                          0.01267817, 0.01004756, 0.00738993, 0.00471273, 0.00202681};

const double roots[] = {-0.99921012, -0.99584053, -0.9897879 , -0.9810672 , -0.96970179,
                        -0.95572226, -0.93916628, -0.92007848, -0.89851031, -0.87451992,
                        -0.84817198, -0.81953753, -0.78869374, -0.75572378, -0.72071651,
                        -0.68376633, -0.64497283, -0.6044406 , -0.5622789 , -0.5186014 ,
                        -0.47352584, -0.42717374, -0.37967006, -0.33114285, -0.28172294,
                        -0.23154355, -0.18073996, -0.12944914, -0.07780933, -0.02595977,
                        0.02595977,  0.07780933,  0.12944914,  0.18073996,  0.23154355,
                        0.28172294,  0.33114285,  0.37967006,  0.42717374,  0.47352584,
                        0.5186014 ,  0.5622789 ,  0.6044406 ,  0.64497283,  0.68376633,
                        0.72071651,  0.75572378,  0.78869374,  0.81953753,  0.84817198,
                        0.87451992,  0.89851031,  0.92007848,  0.93916628,  0.95572226,
                        0.96970179,  0.9810672 ,  0.9897879 ,  0.99584053,  0.99921012};

const int deg = 60;  // length of weights/roots

const double COSH_LIMIT = 100;

const char DblBknEdgeOn::className[] = "DblBknEdgeOn";


/* ---------------- Local Functions ------------------------------------ */

double LuminosityDensityBED(double s, void *params);

double Quad(double J_0, double R, double h, double b);

double Quad_double(double J_0, double R, double h, double a, double b);


/* ---------------- CONSTRUCTOR ---------------------------------------- */

DblBknEdgeOn::DblBknEdgeOn() {
    string paramName;

    nParams = N_PARAMS;
    functionName = FUNCTION_NAME;
    shortFunctionName = className;

    // Set up the vector of parameter labels
    for (int i = 0; i < nParams; i++) {
        paramName = PARAM_LABELS[i];
        parameterLabels.push_back(paramName);
    }

    doSubsampling = false;
}


/* ---------------- PUBLIC METHOD: Setup ------------------------------- */

void DblBknEdgeOn::Setup(double params[], int offsetIndex, double xc, double yc) {
    x0 = xc;
    y0 = yc;
    PA = params[0 + offsetIndex];
    J_0 = params[1 + offsetIndex];                     // J_0 - intensity for inner exp. disk, 3D-intensity
    h1 = params[2 + offsetIndex];
    h2 = params[3 + offsetIndex];
    h3 = params[4 + offsetIndex];
    r_b_1 = params[5 + offsetIndex];
    r_b_2 = params[6 + offsetIndex];
    n = params[7 + offsetIndex];
    z_0 = params[8 + offsetIndex];

    // convert PA to +x-axis reference
    PA_rad = (PA + 90.0) * DEG2RAD;
    cosPA = cos(PA_rad);
    sinPA = sin(PA_rad);

    // broken-exponential stuff
    J_0_2 = J_0 * exp(r_b_1 / h2 - r_b_1 / h1);     // J_0_2 - intensity for middle exp. disk
    J_0_3 = J_0_2 * exp(r_b_2 / h3 - r_b_2 / h2);   // J_0_3 - intensity for outer exp. disk
    J_0_3K = 2 * J_0_3 * h3;                           // J_0_3 - 3D-intensity, J_0_3K - surface intensity

    // vertical-profile stuff
    alphaVert = 2.0 / n;
    scaledZ0 = alphaVert * z_0;
    two_to_alpha = pow(2.0, alphaVert);

}

/* ---------------- PUBLIC METHOD: GetValue ---------------------------- */


double DblBknEdgeOn::GetValue(double x, double y) {

    double x_diff = x - x0;
    double y_diff = y - y0;
    double totalIntensity;

    // Calculate x,y in component (projected sky) reference frame
    double R = x_diff * cosPA + y_diff * sinPA;
    double z = -x_diff * sinPA + y_diff * cosPA;

    R = fabs(R);
    z = fabs(z);

    double K1 = cyl_bessel_k(1, R / h3);
    double J3 = J_0_3K * R / h3 * K1;
    double Rh = R / h3;
    if (R < r_b_1) {
        double layer_1 = sqrt((r_b_1 - R) * (r_b_1 + R));
        double layer_2 = sqrt((r_b_2 - R) * (r_b_2 + R));
        if (Rh < 1e-8) {
            J3 = J_0_3K;
        }
        totalIntensity = Quad(J_0, R, h1, layer_1) + Quad_double(J_0_2, R, h2, layer_1, layer_2) + J3 -
                         Quad(J_0_3, R, h3, layer_2);
    } else if (R < r_b_2) {
        double layer_2 = sqrt((r_b_2 - R) * (r_b_2 + R));
        totalIntensity = Quad(J_0_2, R, h2, layer_2) + J3 - Quad(J_0_3, R, h3, layer_2);
    } else {
        totalIntensity = J3;
    }

    //if combination of n*z/z_0 is large enough, switch to simple exponential
    if ((z / scaledZ0) > COSH_LIMIT)
        return totalIntensity * two_to_alpha * exp(-z / z_0);
    else {
        double sech = 1.0 / cosh(z / scaledZ0);
        return totalIntensity * pow(sech, alphaVert);
    }

}

/* ----------------------------- OTHER FUNCTIONS -------------------------------- */


double Quad_double(double J_0, double R, double h, double a, double b) {
    double sum = 0;
    for (int i = 0; i < deg; i++) {
        sum += weights[i] * exp(-sqrt(pow(R, 2) + pow((b - a) / 2 * (roots[i] + 1) + a, 2)) / h);
    }

    return sum * J_0 * (b - a);
}

/* END OF FILE: func_double-brokenexpdisk3D-edgeon.cpp ----------------------------------- */
