/* FILE: func_brokenexpdisk3D-edgeon.cpp ------------------------------------------ */



/* ------------------------ Include Files (Header Files )--------------- */
#include <cmath>
#include <string>
#include "func_brokenexpdisk3D-edgeon.h"

using namespace std;

/* ---------------- Definitions ---------------------------------------- */
const int N_PARAMS = 7;
const char PARAM_LABELS[][20] = {"PA", "J_0", "h1", "h2", "r_break", "n", "z_0"};
const char FUNCTION_NAME[] = "BknEdgeOn function";
const double DEG2RAD = 0.017453292519943295;

const double weights[] = {0.00290862, 0.0067598, 0.01059055, 0.01438082, 0.01811556,
                          0.02178024, 0.02536067, 0.02884299, 0.03221373, 0.03545984,
                          0.03856876, 0.04152846, 0.0443275, 0.04695505, 0.04940094,
                          0.0516557, 0.05371062, 0.05555774, 0.05718993, 0.05860085,
                          0.05978506, 0.06073797, 0.0614559, 0.06193607, 0.06217662,
                          0.06217662, 0.06193607, 0.0614559, 0.06073797, 0.05978506,
                          0.05860085, 0.05718993, 0.05555774, 0.05371062, 0.0516557,
                          0.04940094, 0.04695505, 0.0443275, 0.04152846, 0.03856876,
                          0.03545984, 0.03221373, 0.02884299, 0.02536067, 0.02178024,
                          0.01811556, 0.01438082, 0.01059055, 0.0067598, 0.00290862};

const double roots[] = {-0.9988664, -0.99403197, -0.98535408, -0.97286439, -0.95661096,
                        -0.93665662, -0.91307856, -0.88596798, -0.85542977, -0.82158207,
                        -0.78455583, -0.7444943, -0.70155247, -0.65589647, -0.60770293,
                        -0.5571583, -0.50445814, -0.44980633, -0.39341431, -0.33550025,
                        -0.27628819, -0.21600724, -0.15489059, -0.0931747, -0.03109834,
                        0.03109834, 0.0931747, 0.15489059, 0.21600724, 0.27628819,
                        0.33550025, 0.39341431, 0.44980633, 0.50445814, 0.5571583,
                        0.60770293, 0.65589647, 0.70155247, 0.7444943, 0.78455583,
                        0.82158207, 0.85542977, 0.88596798, 0.91307856, 0.93665662,
                        0.95661096, 0.97286439, 0.98535408, 0.99403197, 0.9988664};

const int deg = 50;  // length of weights/roots

const double COSH_LIMIT = 100;

const char BknEdgeOn::className[] = "BknEdgeOn";


/* ---------------- Local Functions ------------------------------------ */

double LuminosityDensityBED(double s, void *params);

double Quad(double J_0, double R, double h, double b);


/* ---------------- CONSTRUCTOR ---------------------------------------- */

BknEdgeOn::BknEdgeOn() {
    string paramName;

    nParams = N_PARAMS;
    functionName = FUNCTION_NAME;
    shortFunctionName = className;

    // Set up the vector of parameter labels
    for (int i = 0; i < nParams; i++) {
        paramName = PARAM_LABELS[i];
        parameterLabels.push_back(paramName);
    }

    doSubsampling = false;
}


/* ---------------- PUBLIC METHOD: Setup ------------------------------- */

void BknEdgeOn::Setup(double params[], int offsetIndex, double xc, double yc) {
    x0 = xc;
    y0 = yc;
    PA = params[0 + offsetIndex];
    J_0 = params[1 + offsetIndex];              // J_0 - intensity for inner exp. disk, 3D-intensity
    h1 = params[2 + offsetIndex];
    h2 = params[3 + offsetIndex];
    r_b = params[4 + offsetIndex];
    n = params[5 + offsetIndex];
    z_0 = params[6 + offsetIndex];

    // convert PA to +x-axis reference
    PA_rad = (PA + 90.0) * DEG2RAD;
    cosPA = cos(PA_rad);
    sinPA = sin(PA_rad);

    // broken-exponential stuff
    J_0_2 = J_0 * exp(r_b / h2 - r_b / h1);  // J_0_2 - intensity for outer exp. disk
    J_0_2K = 2 * J_0_2 * h2;                    // J_0_2 - 3D-intensity, J_0_2K - surface intensity

    // vertical-profile stuff
    alphaVert = 2.0 / n;
    scaledZ0 = alphaVert * z_0;
    two_to_alpha = pow(2.0, alphaVert);

}

/* ---------------- PUBLIC METHOD: GetValue ---------------------------- */


double BknEdgeOn::GetValue(double x, double y) {

    double x_diff = x - x0;
    double y_diff = y - y0;
    double totalIntensity;

    // Calculate x,y in component (projected sky) reference frame
    double R = x_diff * cosPA + y_diff * sinPA;
    double z = -x_diff * sinPA + y_diff * cosPA;

    R = fabs(R);
    z = fabs(z);

    double K1 = cyl_bessel_k(1, R / h2);
    double J = J_0_2K * R / h2 * K1;
    double Rh = R / h2;
    if (R < r_b) {
        double layer = sqrt((r_b - R) * (r_b + R));
        if (Rh < 1e-8) {
            J = J_0_2K;
        }
        totalIntensity = Quad(J_0, R, h1, layer) + J - Quad(J_0_2, R, h2, layer);
    } else {
        totalIntensity = J;
    }

    //if combination of n*z/z_0 is large enough, switch to simple exponential
    if ((z / scaledZ0) > COSH_LIMIT)
        return totalIntensity * two_to_alpha * exp(-z / z_0);
    else {
        double sech = 1.0 / cosh(z / scaledZ0);
        return totalIntensity * pow(sech, alphaVert);
    }

}


/* ----------------------------- OTHER FUNCTIONS -------------------------------- */


double Quad(double J_0, double R, double h, double b) {
    double sum = 0;
    for (int i = 0; i < deg; i++) {
        sum += weights[i] * exp(-sqrt(pow(R, 2) + pow(b / 2 * (roots[i] + 1), 2)) / h);
    }

    return sum * J_0 * b;
}


/* END OF FILE: func_brokenexpdisk3D-edgeon.cpp ----------------------------------- */
